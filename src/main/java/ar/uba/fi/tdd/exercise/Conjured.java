package ar.uba.fi.tdd.exercise;

public class Conjured extends Item {

    public Conjured(String _name, int _sellIn, int _quality) {
        super(_name, _sellIn, _quality);
    }

    @Override
    public void updateQuality() {
        if (this.quality < 50 && this.quality > 0) {
            this.quality -= 2;
            if (this.quality < 0) {
                this.quality = 0;
            }
        }

        this.sellIn--;
    }
 }
