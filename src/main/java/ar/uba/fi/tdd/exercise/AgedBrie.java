package ar.uba.fi.tdd.exercise;

public class AgedBrie extends Item {
    
    public AgedBrie(String _name, int _sellIn, int _quality) {
        super(_name, _sellIn, _quality);
    }

    @Override
    public void updateQuality() {
        this.sellIn--;

        if (this.quality < 50) {
            this.quality++;

            if (this.sellIn < 0 && this.quality < 50) {
                this.quality++;
            }
        }
    }
 }
