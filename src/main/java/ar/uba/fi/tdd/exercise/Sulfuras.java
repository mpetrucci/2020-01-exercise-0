package ar.uba.fi.tdd.exercise;

public class Sulfuras extends Item {
    public static int SULFURAS_CONSTANT_QUALITY = 80;

    public Sulfuras(String _name) {
        super(_name, 0, Sulfuras.SULFURAS_CONSTANT_QUALITY);
    }
 }
