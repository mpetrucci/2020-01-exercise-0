package ar.uba.fi.tdd.exercise;

public class BackstagePasses extends Item {

    public BackstagePasses(String _name, int _sellIn, int _quality) {
        super(_name, _sellIn, _quality);
    }

    @Override
    public void updateQuality() {
        if (this.quality < 50) {
            if (this.sellIn <= 5) {
                this.quality += 2;
            }
            else if (this.sellIn <= 10) {
                this.quality += 1;
            }

            if (this.quality > 50) {
                this.quality = 50;
            }
        }

        this.sellIn--;

        if (this.sellIn < 0) {
            this.quality = 0;
        }
    }
 }
