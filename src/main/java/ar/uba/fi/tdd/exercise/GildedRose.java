
package ar.uba.fi.tdd.exercise;

class GildedRose {
    protected Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            items[i].updateQuality();
        }
    }
}
