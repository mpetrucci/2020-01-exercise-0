package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void itemQualityShouldBePositive() {
			int initialQuality = -1;
			Item[] items = new Item[] { new AgedBrie("Aged Brie", 0, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isPositive();
	}

	@Test
	public void testAgedBrieItemsQualityShouldPassFifty() {
			int initialQuality = 49;
			Item[] items = new Item[] { new AgedBrie("Aged Brie", 3, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			app.updateQuality();
			assertThat(items[0].quality).isEqualTo(50);
	}

	@Test
	public void testAgedBrieItemsShouldIncreaseQuality() {
			int initialQuality = 5;
			Item[] items = new Item[] { new AgedBrie("Aged Brie", 0, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isGreaterThan(initialQuality);
	}

	@Test
	public void testSulfurasItemsShouldHasConstantQuality() {
			Item[] items = new Item[] { new Sulfuras("Sulfuras") };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isEqualTo(Sulfuras.SULFURAS_CONSTANT_QUALITY);
	}

	@Test
	public void testBackstagePassesItemsShouldIncreaseQuality() {
			int initialQuality = 5;
			Item[] items = new Item[] { new BackstagePasses("Backstage Pass to X concert", 3, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isGreaterThan(initialQuality);
	}

	@Test
	public void testBackstagePassesItemsShouldDropQualityToZero() {
			int initialQuality = 35;
			Item[] items = new Item[] { new BackstagePasses("Backstage Pass to X concert", 0, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isZero();
	}

	@Test
	public void testConjuredItemsShouldDropTwiceAsFastAsNormal() {
			int initialQuality = 35;
			Item[] items = new Item[] { new Conjured("Conjured", 4, initialQuality) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(items[0].quality).isEqualTo(initialQuality - 2);
	}
}
